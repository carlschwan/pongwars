// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: MIT

#pragma once

#include <QtQml>
#include <QQuickItem>

class QSGGeometryNode;

class Scene : public QQuickItem
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(int pointLight READ pointLight NOTIFY pointChanged)
    Q_PROPERTY(int pointDark READ pointDark NOTIFY pointChanged)
    Q_PROPERTY(bool gravity READ gravity WRITE setGravity NOTIFY gravityChanged)
    Q_PROPERTY(bool paddleVisible READ paddleVisible WRITE setPaddleVisible NOTIFY paddleVisibleChanged)

public:
    explicit Scene(QQuickItem *parent = nullptr);
    ~Scene();

    int pointLight() const;
    int pointDark() const;

    bool gravity() const;
    void setGravity(bool gravitiy);

    bool paddleVisible() const;
    void setPaddleVisible(bool paddleVisible);

Q_SIGNALS:
    void pointChanged();
    void gravityChanged();
    void paddleVisibleChanged();

protected:
    QSGNode *updatePaintNode(QSGNode *, QQuickItem::UpdatePaintNodeData *) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    struct Ball {
        QColor color;
        QColor backgroundColor;
        double x;
        double y;
        double dx;
        double dy;
        QSGGeometryNode *node = nullptr;
    };

    struct Paddle {
        QRectF position;
        double speed = 0.0;
        QSGGeometryNode *node = nullptr;
    };

    QSGNode *createRootNode();
    QSGGeometryNode *createSquareNode(const QRectF &rect, const QColor &color);
    void createBallNode(Ball &ball);
    void updateBallPosition(const Ball &ball);
    void updatePaddlePosition(const Paddle &paddle);
    void updateSquareAndBounce(Ball &ball);

    void tick();

    Ball m_ballLight;
    Ball m_ballDark;
    QList<QList<QColor>> m_squareColors;
    QList<QList<QSGGeometryNode *>> m_squares;
    int m_numSquaresX = 0;
    int m_numSquaresY = 0;

    int m_pointLight = 0;
    int m_pointDark = 0;

    bool m_gravity = false;
    bool m_paddleVisible = false;

    Paddle m_paddleLeft;
    Paddle m_paddleRight;

    QTimer m_timer;
};
