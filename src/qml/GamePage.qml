// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import PongWars

Kirigami.Page {
    id: root

    property alias paddleVisible: scene.paddleVisible

    title: i18nc("@info", "Day %1 | Night %2", scene.pointLight, scene.pointDark)

    padding: 0

    actions: [
        Kirigami.Action {
            icon.name: 'globe-symbolic'
            text: i18nc("@action:button", "Gravity")
            checkable: true
            checked: scene.gravity
            onCheckedChanged: (checked) => {
                scene.gravity = checked;
            }
        },
        Kirigami.Action {
            icon.name: 'eu.carlschwan.pongwars'
            text: i18nc("@action:button", "About")
            onTriggered: () => {
                applicationWindow().pageStack.pushDialogLayer(Qt.createComponent('PongWars', 'About.qml'), {}, {
                    width: Kirigami.Units.gridUnit * 15
                });
            }
        }
    ]

    Scene {
        id: scene

        anchors.fill: parent
        focus: true
    }
}

