// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import PongWars

FormCard.FormCardPage {
    id: root

    Kirigami.Icon {
        source: "eu.carlschwan.pongwars"
        Layout.topMargin: Kirigami.Units.largeSpacing
        Layout.alignment: Qt.AlignHCenter
        implicitWidth: Math.round(Kirigami.Units.iconSizes.huge * 1.5)
        implicitHeight: Math.round(Kirigami.Units.iconSizes.huge * 1.5)
    }

    Kirigami.Heading {
        text: i18n("Welcome to Pong Wars")

        Layout.alignment: Qt.AlignHCenter
        Layout.topMargin: Kirigami.Units.largeSpacing
    }

    FormCard.FormHeader {
        title: i18nc("@title:group", "Mode")
    }

    FormCard.FormCard {
        FormCard.FormButtonDelegate {
            id: flashcardRadio

            text: i18nc("@action:button Mode selector", "Animation")
            onClicked: applicationWindow().pageStack.push(Qt.createComponent('PongWars', 'GamePage'), {
                paddleVisible: false,
            });
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormButtonDelegate {
            id: multipleChoiceRadio

            text: i18nc("@option:check Mode selector", "2-Player")
            onClicked: applicationWindow().pageStack.push(Qt.createComponent('PongWars', 'GamePage'), {
                paddleVisible: true,
            });
        }
    }
}

