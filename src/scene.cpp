// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: MIT
// Based on code by Koen van Gilst

#include "scene.h"

#include <QSGGeometryNode>
#include <QSGFlatColorMaterial>

using namespace Qt::Literals::StringLiterals;
using namespace std::chrono;

constexpr int squareSize = 25.0;
constexpr int numPoints = 20;

namespace {
double randomNum(double min, double max) {
    return QRandomGenerator::system()->generateDouble() * (max - min) + min;
}
}

Scene::Scene(QQuickItem *parent)
    : QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents);

    connect(&m_timer, &QTimer::timeout, this, &Scene::tick);
    m_timer.setTimerType(Qt::PreciseTimer);
    m_timer.start(1000ms);
}

Scene::~Scene() = default;

QSGGeometryNode *Scene::createSquareNode(const QRectF &rect, const QColor &color)
{
    auto node = new QSGGeometryNode;
    const int vertexCount = 4;
    const int indexCount = 2 * 3;
    QSGGeometry *geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), vertexCount, indexCount);
    geometry->setDrawingMode(QSGGeometry::DrawTriangles);

    geometry->vertexDataAsPoint2D()[0].set(rect.x(), rect.y());
    geometry->vertexDataAsPoint2D()[1].set(rect.x() + rect.width(), rect.y());
    geometry->vertexDataAsPoint2D()[2].set(rect.x() + rect.width(), rect.y() + rect.height());
    geometry->vertexDataAsPoint2D()[3].set(rect.x(), rect.y() + rect.height());

    quint16 *indices = geometry->indexDataAsUShort();
    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 0;
    indices[4] = 3;
    indices[5] = 2;

    node->setGeometry(geometry);
    node->setFlag(QSGNode::OwnsGeometry);
    node->setFlag(QSGNode::OwnsMaterial);

    auto material = new QSGFlatColorMaterial;
    material->setColor(color);
    node->setMaterial(material);

    return node;
}

void Scene::createBallNode(Ball &ball)
{
    ball.node = new QSGGeometryNode();
    QSGGeometry *geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), numPoints, (numPoints - 2) * 3);
    geometry->setDrawingMode(QSGGeometry::DrawTriangles);
    ball.node->setGeometry(geometry);
    ball.node->setFlag(QSGNode::OwnsGeometry);
    ball.node->setFlag(QSGNode::OwnsMaterial);

    updateBallPosition(ball);

    auto material = new QSGFlatColorMaterial;
    material->setColor(ball.color);
    ball.node->setMaterial(material);
}

void Scene::updateBallPosition(const Ball &ball)
{
    constexpr float angle = 360.0f / numPoints;
    constexpr int triangleCount = numPoints - 2;

    const QRectF rect(ball.x, ball.y, squareSize / 2.0, squareSize / 2.0);

    float radius = rect.width();
    auto geometry = ball.node->geometry();
    quint16 *indices = geometry->indexDataAsUShort();

    for (int i = 0; i < numPoints; ++i) {
        float currentAngle = angle * i;
        float theta = currentAngle * M_PI / 180;
        geometry->vertexDataAsPoint2D()[i].x = rect.x() + radius * cos(theta);
        geometry->vertexDataAsPoint2D()[i].y = rect.y() + radius * sin(theta);
    }

    for (int i = 0; i < triangleCount; i++) {
        indices[i * 3] = 0;
        indices[i * 3 + 1] = i + 1;
        indices[i * 3 + 2] = i + 2;
    }

    ball.node->markDirty(QSGNode::DirtyGeometry);
}

void Scene::updatePaddlePosition(const Paddle &paddle)
{
    auto geometry = paddle.node->geometry();
    const auto &rect = paddle.position;

    geometry->vertexDataAsPoint2D()[0].set(rect.x(), rect.y());
    geometry->vertexDataAsPoint2D()[1].set(rect.x() + rect.width(), rect.y());
    geometry->vertexDataAsPoint2D()[2].set(rect.x() + rect.width(), rect.y() + rect.height());
    geometry->vertexDataAsPoint2D()[3].set(rect.x(), rect.y() + rect.height());

    paddle.node->markDirty(QSGNode::DirtyGeometry);
}

QSGNode *Scene::createRootNode()
{
    m_ballDark = Ball{
        QColor(u"#114C5A"_s),
        QColor(u"#D9E8E3"_s),
        width() / 4.0,
        height() / 2.0,
        12.5 + randomNum(-1.15, 1.15),
        -12.5 + randomNum(-1.15, 1.25),
    };

    m_ballLight = Ball{
        QColor(u"#D9E8E3"_s),
        QColor(u"#114C5A"_s),
        width() / 4.0 * 3.0,
        height() / 2.0,
        -12.5 + randomNum(-1.25, 1.25),
        -12.5 + randomNum(-1.25, 1.25),
    };

    auto root = createSquareNode(QRectF(0, 0, width(), height()), QColor(Qt::transparent));

    m_numSquaresX = width() / squareSize;
    m_numSquaresY = height() / squareSize;

    for (auto i = 0; i < m_numSquaresX; i++) {
        m_squares.append(QList<QSGGeometryNode *>{});
        m_squareColors.append(QList<QColor>{});

        for (auto j = 0; j < m_numSquaresY; j++) {
            const QRectF rect(static_cast<double>(i) * squareSize, static_cast<double>(j) * squareSize, squareSize, squareSize);

            const auto color = i < m_numSquaresX / 2 ? m_ballDark.backgroundColor : m_ballLight.backgroundColor;
            auto square = createSquareNode(rect, color);
            square->markDirty(QSGNode::DirtyGeometry);
            root->appendChildNode(square);
            m_squares[i].append(square);
            m_squareColors[i].append(color);
        }
    }

    root->markDirty(QSGNode::DirtyGeometry);

    createBallNode(m_ballLight);
    createBallNode(m_ballDark);
    root->appendChildNode(m_ballLight.node);
    root->appendChildNode(m_ballDark.node);

    if (m_paddleVisible) {
        m_paddleLeft.position = QRectF{10, height() / 2.0 - 200 / 2.0, 10, 200};
        m_paddleLeft.node = createSquareNode(m_paddleLeft.position, QColor(Qt::black));
        m_paddleLeft.node->markDirty(QSGNode::DirtyGeometry);
        root->appendChildNode(m_paddleLeft.node);

        m_paddleRight.position = QRectF{width() - 20, height() / 2.0 - 200 / 2.0, 10, 200};

        m_paddleRight.node = createSquareNode(m_paddleRight.position, QColor(Qt::black));
        m_paddleRight.node->markDirty(QSGNode::DirtyGeometry);
        root->appendChildNode(m_paddleRight.node);
    }

    return root;
}

QSGNode *Scene::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData* updatePaintNodeData)
{
    Q_UNUSED(updatePaintNodeData)

    if(!oldNode) {
        return createRootNode();
    }

    auto root = static_cast<QSGGeometryNode *>(oldNode);
    updateBallPosition(m_ballLight);
    updateBallPosition(m_ballDark);

    for (auto i = 0; i < m_numSquaresX; i++) {
        for (auto j = 0; j < m_numSquaresY; j++) {
            auto node = m_squares[i][j];
            auto material = static_cast<QSGFlatColorMaterial *>(node->material());
            auto color = m_squareColors[i][j];
            if (material->color() != color) {
                material->setColor(color);
                node->markDirty(QSGNode::DirtyMaterial);
            }
        }
    }

    if (m_paddleVisible) {
        if (m_paddleLeft.speed != 0) {
            updatePaddlePosition(m_paddleLeft);
        }
        if (m_paddleRight.speed != 0) {
            updatePaddlePosition(m_paddleRight);
        }
    }

    return root;
}

void capSpeed(double &dx, double &dy)
{
    const auto maxSpeed = 15;
    const auto speed = sqrt(dx * dx + dy * dy);
    if (speed > maxSpeed) {
        dx = (dx / speed) * maxSpeed;
        dy = (dy / speed) * maxSpeed;
    }
}

// Based on https://github.com/vnglst/pong-wars/blob/1539c647493f5e0184737c577960f829abfd2f41/index.html#L140
void Scene::updateSquareAndBounce(Ball &ball) {
    double checkX = ball.x + ball.dx;
    double checkY = ball.y + ball.dy;

    double i = floor(ball.x / squareSize);
    double j = floor(ball.y / squareSize);
    double ci = floor(checkX / squareSize);
    double cj = floor(checkY / squareSize);

    bool scoreUpdated = false;

    if (ci >= 0 && ci < m_numSquaresX && cj >= 0 && cj < m_numSquaresY) {
        const auto color1 = m_squareColors[ci][j];
        if (color1 != ball.backgroundColor) {
            m_squareColors[ci][j] = ball.backgroundColor;
            scoreUpdated = true;

            ball.dx = -ball.dx;
        }

        const auto color2 = m_squareColors[i][cj];
        if (color2 != ball.backgroundColor) {
            m_squareColors[i][cj] = ball.backgroundColor;
            ball.dy = -ball.dy;
        }

        ball.dx += randomNum(-0.01, 0.01);
        ball.dx += randomNum(-0.01, 0.01);
    }

    if (scoreUpdated) {
        m_pointLight = 0;
        m_pointDark = 0;
        for (auto i = 0; i < m_numSquaresX; i++) {
            for (auto j = 0; j < m_numSquaresY; j++) {
                auto color = m_squareColors[i][j];
                if (color == m_ballLight.color) {
                    m_pointLight++;
                } else {
                    m_pointDark++;
                }
            }
        }
        Q_EMIT pointChanged();
    }

    if (ball.x + ball.dx > width() - squareSize / 2.0 || ball.x + ball.dx < squareSize / 2.0) {
        ball.dx = -ball.dx;
    }
    if (ball.y + ball.dy > height() - squareSize / 2.0 || ball.y + ball.dy < squareSize / 2.0) {
        ball.dy = -ball.dy;
    }

    if (m_gravity) {
        const auto gravity = 0.3; // Strength of gravity
        // Apply gravitational pull towards the center of the canvas
        if (ball.y < (height() / 2)) {
            ball.dy += gravity;
        } else {
            ball.dy -= gravity;
        }

        if (ball.x < (width() / 2)) {
            ball.dx += gravity;
        } else {
            ball.dx -= gravity;
        }
        // Cap the speed to maxSpeed in case gravity gets crazy
        capSpeed(ball.dx, ball.dy);
    } else {
        if (abs(ball.dx) + abs(ball.dy) < 6) {
            ball.dx += ball.dx > 0 ? 0.1 : -0.1;
            ball.dy += ball.dy > 0 ? 0.1 : -0.1;
        } else if (abs(ball.dx) + abs(ball.dy) > 35) {
            ball.dx += ball.dx > 0 ? -0.1 : 0.1;
            ball.dy += ball.dy > 0 ? -0.1 : 0.1;
        }
    }

    if (m_paddleVisible) {
        // Bounce off paddles
        if (ball.x + ball.dx > m_paddleLeft.position.x() && ball.x + ball.dx < m_paddleLeft.position.x() + m_paddleLeft.position.width()
            && ball.y + ball.dy > m_paddleLeft.position.y() && ball.y + ball.dy < m_paddleLeft.position.y() + m_paddleLeft.position.height()) {
            const auto paddleCenterY = m_paddleLeft.position.y() + m_paddleLeft.position.height() / 2;
            const auto distanceFromCenter = m_paddleLeft.position.y() - paddleCenterY;
            const auto normalizedDistance = distanceFromCenter / (m_paddleLeft.position.height() / 2);
            const auto bounceAngle = normalizedDistance * (M_PI / 3);
            ball.dx = -ball.dx;
            ball.dy = tan(bounceAngle) * ball.dy;
        }

        if (ball.x + ball.dx > m_paddleRight.position.x() && ball.x + ball.dx < m_paddleRight.position.x() + m_paddleRight.position.width()
            && ball.y + ball.dy > m_paddleRight.position.y() && ball.y + ball.dy < m_paddleRight.position.y() + m_paddleRight.position.height()) {
            const auto paddleCenterY = m_paddleRight.position.y() + m_paddleRight.position.height() / 2;
            const auto distanceFromCenter = m_paddleRight.position.y() - paddleCenterY;
            const auto normalizedDistance = distanceFromCenter / (m_paddleRight.position.height() / 2);
            const auto bounceAngle = normalizedDistance * (M_PI / 3);
            ball.dx = -ball.dx;
            ball.dy = tan(bounceAngle) * ball.dx;
        }

        m_paddleLeft.position.translate(0, m_paddleLeft.speed);
        if (m_paddleLeft.position.y() < 0) {
            m_paddleLeft.position.translate(0, -m_paddleLeft.position.y());
        } else if (m_paddleLeft.position.bottom() > m_numSquaresY * squareSize) {
            m_paddleLeft.position.translate(0, m_numSquaresY * squareSize - m_paddleLeft.position.bottom());
        }
        m_paddleRight.position.translate(0, m_paddleRight.speed);

        if (m_paddleRight.position.y() < 0) {
            m_paddleRight.position.translate(0, -m_paddleRight.position.y());
        } else if (m_paddleRight.position.bottom() > m_numSquaresY * squareSize) {
            m_paddleRight.position.translate(0, m_numSquaresY * squareSize - m_paddleRight.position.bottom());
        }
    }

    ball.x += ball.dx;
    ball.y += ball.dy;
}

void Scene::tick()
{
    updateSquareAndBounce(m_ballLight);
    updateSquareAndBounce(m_ballDark);

    m_timer.start(1000ms / 60);

    update();
}

int Scene::pointLight() const
{
    return m_pointLight;
}

int Scene::pointDark() const
{
    return m_pointDark;
}

bool Scene::gravity() const
{
    return m_gravity;
}

void Scene::setGravity(bool gravity)
{
    if (m_gravity == gravity) {
        return;
    }
    m_gravity = gravity;
    Q_EMIT gravityChanged();
}

bool Scene::paddleVisible() const
{
    return m_paddleVisible;
}

void Scene::setPaddleVisible(bool paddleVisible)
{
    if (m_paddleVisible == paddleVisible) {
        return;
    }
    m_paddleVisible = paddleVisible;
    Q_EMIT paddleVisibleChanged();
}

void Scene::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Down:
        m_paddleRight.speed = 4.0;
        break;
    case Qt::Key_Up:
        m_paddleRight.speed = -4.0;
        break;
    case Qt::Key_W:
        m_paddleLeft.speed = -4.0;
        break;
    case Qt::Key_S:
        m_paddleLeft.speed = 4.0;
        break;
    }
    QQuickItem::keyPressEvent(event);
}

void Scene::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Down:
        m_paddleRight.speed = 0;
        break;
    case Qt::Key_Up:
        m_paddleRight.speed = 0;
        break;
    case Qt::Key_W:
        m_paddleLeft.speed = 0;
        break;
    case Qt::Key_S:
        m_paddleLeft.speed = 0;
        break;
    }
    QQuickItem::keyReleaseEvent(event);
}
