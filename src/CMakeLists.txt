# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

qt_add_library(pongwars_static STATIC)
qt_add_qml_module(pongwars_static
    URI
        PongWars
    QML_FILES
        qml/About.qml
        qml/Main.qml
        qml/WelcomePage.qml
        qml/GamePage.qml
    SOURCES
        app.cpp
        app.h
        scene.cpp
        scene.h
    VERSION
        1.0
)

target_link_libraries(pongwars_static PUBLIC
    Qt6::Core
    Qt6::Gui
    Qt6::Qml
    Qt6::Quick
    Qt6::QuickControls2
    Qt6::Svg
    KF6::I18n
    KF6::CoreAddons
    KF6::ConfigCore
    KF6::ConfigGui)

if (ANDROID)
    kirigami_package_breeze_icons(ICONS
        list-add
        help-about
        application-exit
        applications-graphics
    )
else()
    target_link_libraries(pongwars_static PUBLIC Qt::Widgets)
endif()


kconfig_add_kcfg_files(pongwars_static GENERATE_MOC pongwarsconfig.kcfgc)

add_executable(pongwars main.cpp)
target_link_libraries(pongwars PRIVATE pongwars_static pongwars_staticplugin)

install(TARGETS pongwars ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
